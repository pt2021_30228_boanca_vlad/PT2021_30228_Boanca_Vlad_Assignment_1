package to.tuc.tp.tema1;

import to.tuc.tp.tema1.GUI.CalcController;
import to.tuc.tp.tema1.GUI.CalcView;

public class App
{
    public static void main( String[] args )
    {
        CalcView view = new CalcView();
        CalcController controller = new CalcController(view);
    }
}
