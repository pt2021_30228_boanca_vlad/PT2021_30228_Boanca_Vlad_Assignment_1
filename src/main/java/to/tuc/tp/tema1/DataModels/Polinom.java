package to.tuc.tp.tema1.DataModels;

import java.util.LinkedList;
import java.util.List;

public class Polinom {
    private List<Monom> polinom;
    private Integer gradMax;

    public Polinom() {
        polinom = new LinkedList<>();
        gradMax = 0;
    }

    public List<Monom> getListaMonoame() {
        return polinom;
    }

    public void setPolinom(Polinom polinom) {
        this.gradMax = polinom.gradMax;
        this.polinom = polinom.polinom;
    }
    public Integer getGradMax() {
        return this.gradMax;
    }

    public void adaugaMonom(Monom monom) {
        if(monom.getCoeficient().doubleValue() != 0) {
            polinom.add(monom);
            if (monom.getGrad() > gradMax)
                gradMax = monom.getGrad();
        }
    }

    public String toString() {
        StringBuilder rezultat = new StringBuilder();
        for(Monom monom : polinom) {
            if(monom.getCoeficient().doubleValue() < 0)
                rezultat.append(" - ");
            else if(!monom.getGrad().equals(gradMax))
                rezultat.append(" + ");
            rezultat.append(monom.toString());
        }
        return rezultat.toString();
    }
}
