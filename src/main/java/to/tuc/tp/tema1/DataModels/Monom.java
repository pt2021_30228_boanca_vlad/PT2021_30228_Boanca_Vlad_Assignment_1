package to.tuc.tp.tema1.DataModels;

import java.text.DecimalFormat;

public class Monom implements Comparable<Monom> {
    private Number coeficient;
    private final Integer grad;

    public Monom(Number coeficient, Integer grad) {
        this.coeficient = coeficient;
        this.grad = grad;
    }

    public void setCoeficient(Number coeficient) {
        this.coeficient = coeficient;
    }

    public Number getCoeficient() {
        return this.coeficient;
    }

    public Integer getGrad() {
        return this.grad;
    }

    public String toString() {
        String rezultat = "";
        DecimalFormat df = new DecimalFormat("0.00");
        if ((coeficient.doubleValue() == Math.floor(coeficient.doubleValue())) && !Double.isInfinite(coeficient.doubleValue())) {
            if(coeficient.intValue() == 0)
                rezultat = "";
            else if(coeficient.intValue() == 1 || coeficient.intValue() == -1) {
                if(grad == 0)
                    rezultat += 1;
                else
                    rezultat += "x^" + grad;
            } else if(grad == 0)
                rezultat = "" + Math.abs(coeficient.intValue());
            else rezultat = Math.abs(coeficient.intValue()) + "x^" + grad;
        } else rezultat = df.format(Math.abs(coeficient.doubleValue())) + "x^" + grad;
        return rezultat;
    }

    @Override
    public int compareTo(Monom monom) {
        return monom.grad.compareTo(this.grad);
    }
}
