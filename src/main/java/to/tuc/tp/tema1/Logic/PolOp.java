package to.tuc.tp.tema1.Logic;
import to.tuc.tp.tema1.DataModels.*;
import java.util.Collections;

public class PolOp {

    public static Polinom aduna(Polinom polinom1, Polinom polinom2) {
        Polinom polinomRezultat = new Polinom();
        int gradMax = (polinom1.getGradMax() > polinom2.getGradMax()) ? polinom1.getGradMax() : polinom2.getGradMax();
        Monom[] aux = new Monom[gradMax + 1];
        for(int i = gradMax; i >= 0; i--)
            aux[i] = new Monom(0, i);
        for(Monom monom : polinom1.getListaMonoame())
            aux[monom.getGrad()].setCoeficient(MonOp.aduna(monom, aux[monom.getGrad()]).getCoeficient());
        for(Monom monom : polinom2.getListaMonoame())
            aux[monom.getGrad()].setCoeficient(MonOp.aduna(monom, aux[monom.getGrad()]).getCoeficient());
        for(Monom monom : aux)
            polinomRezultat.adaugaMonom(monom);
        Collections.sort(polinomRezultat.getListaMonoame());
        return polinomRezultat;
    }

    public static Polinom scade(Polinom polinom1, Polinom polinom2) {
        Polinom polinomRezultat = new Polinom();
        int gradMax = (polinom1.getGradMax() > polinom2.getGradMax()) ? polinom1.getGradMax() : polinom2.getGradMax();
        Monom[] aux = new Monom[gradMax + 1];
        for(int i = gradMax; i >= 0; i--)
            aux[i] = new Monom(0, i);
        for(Monom monom : polinom1.getListaMonoame())
            aux[monom.getGrad()].setCoeficient(MonOp.aduna(monom, aux[monom.getGrad()]).getCoeficient());
        for(Monom monom : polinom2.getListaMonoame())
            aux[monom.getGrad()].setCoeficient(MonOp.scade(aux[monom.getGrad()], monom).getCoeficient());
        for(Monom monom : aux)
            polinomRezultat.adaugaMonom(monom);
        Collections.sort(polinomRezultat.getListaMonoame());
        return polinomRezultat;
    }

    public static Polinom inmulteste(Polinom polinom1, Polinom polinom2) {
        Polinom polinomRezultat = new Polinom();
        int gradMax = polinom1.getGradMax() + polinom2.getGradMax();
        Monom[] aux = new Monom[gradMax + 1];
        for(int i = gradMax; i >= 0; i--)
            aux[i] = new Monom(0, i);
        for(Monom monom1 : polinom1.getListaMonoame())
            for(Monom monom2 : polinom2.getListaMonoame()) {
                Monom monomRezultat = MonOp.inmulteste(monom1, monom2);
                aux[monomRezultat.getGrad()] = MonOp.aduna(aux[monomRezultat.getGrad()], monomRezultat);
            }
        for(int i = gradMax; i >= 0; i--)
            polinomRezultat.adaugaMonom(aux[i]);
        return polinomRezultat;
    }

    public static Polinom[] imparte(Polinom polinom1, Polinom polinom2) throws Exception {
        Collections.sort(polinom1.getListaMonoame());
        Collections.sort(polinom2.getListaMonoame());
        Polinom cat = new Polinom();
        Polinom aux = new Polinom();
        Polinom intermediate;
        if(polinom1.getGradMax() < polinom2.getGradMax())
            throw new Exception("Gradul primului polinom trebuie sa fie mai mare sau egal decat gradul celui de-al doilea.");
        int gradDeimpartit = polinom1.getGradMax();
        while(gradDeimpartit >= polinom2.getGradMax()) {
            if(polinom1.getListaMonoame().isEmpty())
                break;
            aux.getListaMonoame().clear();
            Monom m = MonOp.imparte(polinom1.getListaMonoame().get(0), polinom2.getListaMonoame().get(0));
            cat.adaugaMonom(m);
            aux.adaugaMonom(m);
            intermediate = PolOp.inmulteste(aux, polinom2);
            polinom1 = PolOp.scade(polinom1, intermediate);
            gradDeimpartit = polinom1.getGradMax();
        }
        Polinom[] rezultatImpartire = new Polinom[2];
        rezultatImpartire[0] = new Polinom();
        rezultatImpartire[1] = new Polinom();
        rezultatImpartire[0].setPolinom(cat); // catul impartirii
        rezultatImpartire[1].setPolinom(polinom1); // restul impartirii
        return rezultatImpartire;
    }

    public static Polinom deriveaza(Polinom polinom) {
        Polinom rezultat = new Polinom();
        for(Monom monom : polinom.getListaMonoame()) {
        if(monom.getGrad() != 0)
            rezultat.adaugaMonom(new Monom(monom.getCoeficient().intValue() * monom.getGrad(), monom.getGrad() - 1));
        }
        return rezultat;
    }

    public static Polinom integreaza(Polinom polinom) {
        Polinom rezultat = new Polinom();
        for(Monom monom : polinom.getListaMonoame()) {
            rezultat.adaugaMonom(new Monom(monom.getCoeficient().doubleValue() / (monom.getGrad() + 1), monom.getGrad() + 1));
        }
        return rezultat;
    }
}
