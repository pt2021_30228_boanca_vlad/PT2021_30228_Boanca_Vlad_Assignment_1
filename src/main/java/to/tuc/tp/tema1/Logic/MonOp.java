package to.tuc.tp.tema1.Logic;

import to.tuc.tp.tema1.DataModels.Monom;

public class MonOp {
    public static Monom aduna(Monom monom1, Monom monom2) {
        return new Monom(monom1.getCoeficient().intValue() + monom2.getCoeficient().intValue(), monom1.getGrad());
    }

    public static Monom scade(Monom monom1, Monom monom2) {
        return new Monom(monom1.getCoeficient().intValue() - monom2.getCoeficient().intValue(), monom1.getGrad());
    }

    public static Monom inmulteste(Monom monom1, Monom monom2) {
        return new Monom(monom1.getCoeficient().intValue() * monom2.getCoeficient().intValue(), monom1.getGrad() + monom2.getGrad());
    }

    public static Monom imparte(Monom monom1, Monom monom2) {
        return new Monom(monom1.getCoeficient().doubleValue() / monom2.getCoeficient().doubleValue(), monom1.getGrad() - monom2.getGrad());
    }
}
