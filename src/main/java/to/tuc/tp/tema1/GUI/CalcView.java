package to.tuc.tp.tema1.GUI;

import to.tuc.tp.tema1.DataModels.Polinom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class CalcView extends JFrame {
    private final Color green = new Color(136, 247, 158);
    private final JTextField inputPolinom1;
    private final JTextField inputPolinom2;
    private final JTextField inputRezultat;
    private final JTextField inputCat = new JTextField(50);
    private final JTextField inputRest = new JTextField(50);
    private final JTextField inputDeriv1 = new JTextField(50);
    private final JTextField inputDeriv2 = new JTextField(50);
    private final JTextField inputIntegr1 = new JTextField(50);
    private final JTextField inputIntegr2 = new JTextField(50);
    private final JLabel labelRez = new JLabel("Rezultat: ");
    private final JLabel labelCat = new JLabel("Catul: ");
    private final JLabel labelRest = new JLabel("Rest: ");
    private final JLabel labelDeriv1 = new JLabel("Polinomul 1 derivat: ");
    private final JLabel labelDeriv2 = new JLabel("Polinomul 2 derivat: ");
    private final JLabel labelIntegr1 = new JLabel("Polinomul 1 integrat: ");
    private final JLabel labelIntegr2 = new JLabel("Polinomul 2 integrat: ");
    private final JButton[] b = new JButton[6];
    private final JPanel inputPanel;

    public CalcView() {
        inputPolinom1 = new JTextField("x^3 - 2x^2 + 6x - 5", 50);
        inputPolinom2 = new JTextField("x^2 - 1", 50);
        inputRezultat = new JTextField(50);
        inputCat.setEditable(false);
        inputRest.setEditable(false);
        inputDeriv1.setEditable(false);
        inputDeriv2.setEditable(false);
        inputIntegr1.setEditable(false);
        inputIntegr2.setEditable(false);
        inputCat.setBackground(green);
        inputRest.setBackground(green);
        inputDeriv1.setBackground(green);
        inputDeriv2.setBackground(green);
        inputIntegr1.setBackground(green);
        inputIntegr2.setBackground(green);
        this.setTitle("Calculator polinomial");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(650, 500);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        JPanel mainPanel = new JPanel();
        JPanel buttonPanel = new JPanel();
        inputPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        b[0] = new JButton("Aduna");
        b[1] = new JButton("Scade");
        b[2] = new JButton("Inmulteste");
        b[3] = new JButton("Imparte");
        b[4] = new JButton("Deriveaza");
        b[5] = new JButton("Integreaza");
        inputPanel.add(new Label("Polinom 1: "));
        inputPanel.add(inputPolinom1);
        inputPanel.add(new Label("Polinom 2: "));
        inputPanel.add(inputPolinom2);
        inputPanel.add(labelRez);
        inputPanel.add(inputRezultat);
        buttonPanel.add(b[0]);
        buttonPanel.add(b[1]);
        buttonPanel.add(b[2]);
        buttonPanel.add(b[3]);
        buttonPanel.add(b[4]);
        buttonPanel.add(b[5]);
        buttonPanel.setPreferredSize(new Dimension(650, 50));
        mainPanel.add(inputPanel);
        mainPanel.add(buttonPanel);
        this.setContentPane(mainPanel);
        this.setVisible(true);
    }

    protected String getTextPolinom1() {
        return inputPolinom1.getText();
    }
    protected String getTextPolinom2() { return inputPolinom2.getText(); }

    protected void setResult(Polinom rezultat) { // actualizeaza rezultatul pentru adunare, scadere si inmultire
        modifyForStandard();
        inputRezultat.setForeground(Color.black);
        inputRezultat.setBackground(green);
        inputRezultat.setText("" + rezultat);
    }

    protected void setResult(Polinom p1, Polinom p2, String operatie) { // actualizeaza rezultatul pentru derivare si integrare
        modifyForDI(operatie);
        if(operatie.equals("Deriveaza")) {
            inputDeriv1.setText("" + p1);
            inputDeriv2.setText("" + p2);
        } else {
            inputIntegr1.setText("" + p1);
            inputIntegr2.setText("" + p2);
        }
    }

    protected void setResult(Polinom[] p) { // actualizeaza rezultatul pentru impartire
        modifyForDivision();
        inputCat.setText("" + p[0]);
        inputRest.setText("" + p[1]);
    }

    protected void setResult(String mesaj) { // actualizeaza rezultatul in cazul unor exceptii
        modifyForStandard();
        inputRezultat.setText(mesaj);
        inputRezultat.setForeground(Color.white);
        inputRezultat.setBackground(Color.red);
    }

    protected void addListener(ActionListener a) {
        b[0].addActionListener(a);
        b[1].addActionListener(a);
        b[2].addActionListener(a);
        b[3].addActionListener(a);
        b[4].addActionListener(a);
        b[5].addActionListener(a);
    }

    private void modifyForDivision() { // modifica interfata pentru a putea afisa rezultatul impartirii
        clear();
        addDiv();
        update();
    }

    private void modifyForDI(String operatie) { // modifica interfata pentru a putea afisa rezultatul derivarii / integrarii
        clear();
        if(operatie.equals("Deriveaza"))
            addDeriv();
        if(operatie.equals("Integreaza"))
            addIntegr();
        update();
    }

    private void modifyForStandard() { // modifica interfata pentru a putea afisa rezultatele adunarii, scaderii si inmultirii
        clear();
        addStandard();
        update();
    }

    private void addStandard() {
        inputPanel.add(labelRez);
        inputPanel.add(inputRezultat);
    }

    private void removeStandard() {
        inputPanel.remove(labelRez);
        inputPanel.remove(inputRezultat);
    }

    private void addDiv() {
        inputPanel.add(labelCat);
        inputPanel.add(inputCat);
        inputPanel.add(labelRest);
        inputPanel.add(inputRest);
    }

    private void removeDiv() {
        inputPanel.remove(labelCat);
        inputPanel.remove(inputCat);
        inputPanel.remove(labelRest);
        inputPanel.remove(inputRest);
    }

    private void addDeriv() {
        inputPanel.add(labelDeriv1);
        inputPanel.add(inputDeriv1);
        inputPanel.add(labelDeriv2);
        inputPanel.add(inputDeriv2);
    }

    private void removeDeriv() {
        inputPanel.remove(inputDeriv1);
        inputPanel.remove(inputDeriv2);
        inputPanel.remove(labelDeriv1);
        inputPanel.remove(labelDeriv2);
    }

    private void addIntegr() {
        inputPanel.add(labelIntegr1);
        inputPanel.add(inputIntegr1);
        inputPanel.add(labelIntegr2);
        inputPanel.add(inputIntegr2);
    }

    private void removeIntegr() {
        inputPanel.remove(labelIntegr1);
        inputPanel.remove(labelIntegr2);
        inputPanel.remove(inputIntegr1);
        inputPanel.remove(inputIntegr2);
    }

    private void update() {
        inputPanel.repaint();
        inputPanel.revalidate();
    }

    private void clear() {
        removeStandard();
        removeDiv();
        removeIntegr();
        removeDeriv();
    }
}