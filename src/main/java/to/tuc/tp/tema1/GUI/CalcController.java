package to.tuc.tp.tema1.GUI;

import to.tuc.tp.tema1.DataModels.Monom;
import to.tuc.tp.tema1.DataModels.Polinom;
import to.tuc.tp.tema1.Logic.PolOp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalcController {
    private final CalcView c;

    public CalcController(CalcView view) {
        c = view;
        c.addListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Polinom polinomRezultat1 = new Polinom(), polinomRezultat2 = new Polinom();
                String polinom1 = c.getTextPolinom1().replace(" ", "");
                String polinom2 = c.getTextPolinom2().replace(" ", "");
                try {
                    if (!checkForInvalidCharacter(polinom1, polinom2))
                        throw new Exception("Cel putin unul dintre campurile polinoamelor contine caractere invalide.");
                    Pattern pattern = Pattern.compile("(-?\\b\\d+|-)?[xX]\\^(-?\\d+\\b)|(-?\\b\\d+|-)?[xX]|(-?\\b\\d+)");
                    Matcher matcher = pattern.matcher(polinom1);
                    while (matcher.find()) {
                        polinomRezultat1.adaugaMonom(createMonomFromCase(matcher));
                    }
                    matcher = pattern.matcher(polinom2);
                    while (matcher.find()) {
                        polinomRezultat2.adaugaMonom(createMonomFromCase(matcher));
                    }
                    chooseOperation(event.getActionCommand(), polinomRezultat1, polinomRezultat2);
                } catch(Exception exceptie) {
                    c.setResult(exceptie.getMessage());
                }
            }});
    }

    public void chooseOperation(String operatie, Polinom polinom1, Polinom polinom) {
        switch(operatie) {
            case "Aduna": c.setResult(PolOp.aduna(polinom1, polinom)); break;
            case "Scade": c.setResult(PolOp.scade(polinom1, polinom)); break;
            case "Inmulteste": c.setResult(PolOp.inmulteste(polinom1, polinom)); break;
            case "Imparte":
                try {
                    c.setResult(PolOp.imparte(polinom1, polinom));
                } catch(IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    c.setResult("Pentru efectuarea impartirii, al doilea polinom trebuie sa fie diferit de 0!");
                }catch (Exception e) {
                    c.setResult(e.getMessage());
                }
                break;
            case "Deriveaza": c.setResult(PolOp.deriveaza(polinom1), PolOp.deriveaza(polinom), operatie); break;
            case "Integreaza": c.setResult(PolOp.integreaza(polinom1), PolOp.integreaza(polinom), operatie); break;
        }
    }

    private boolean checkForInvalidCharacter(String polinom1, String polinom2) {
        Pattern pattern = Pattern.compile("[^xX0-9-+*^ ]");
        Matcher matcher = pattern.matcher(polinom1);
        boolean found = matcher.find();
        if(found)
            return false;
        matcher = pattern.matcher(polinom2);
        found = matcher.find();
        return !found;
    }

    private int checkMonom(Matcher matcher) {
        if(matcher.group(1) != null && matcher.group(2) != null) {
            if (matcher.group(1).equals("-"))
                return 2;
            return 1;
        }
        if(matcher.group(1) == null && matcher.group(2) != null)
            return 3;
        if(matcher.group(3) != null && matcher.group(4) == null) {
            if(matcher.group(3).equals("-"))
                return 5;
            return 4;
        }
        if(matcher.group(1) == null && matcher.group(2) == null && matcher.group(3) == null && matcher.group(4) == null)
            return 6;
        if(matcher.group(3) == null && matcher.group(4) != null)
            return 7;
        return -1;
    }

    private Monom createMonomFromCase(Matcher matcher) {
        Number coeficient;
        int grad;
        switch (checkMonom(matcher)) {
            case 1: coeficient = Integer.parseInt(matcher.group(1)); grad = Integer.parseInt(matcher.group(2)); break;
            case 2: coeficient = -1; grad = Integer.parseInt(matcher.group(2)); break;
            case 3: coeficient = 1; grad = Integer.parseInt(matcher.group(2)); break;
            case 4: coeficient = Integer.parseInt(matcher.group(3)); grad = 1; break;
            case 5: coeficient = -1; grad = 1; break;
            case 6: coeficient = 1; grad = 1; break;
            case 7: coeficient = Integer.parseInt(matcher.group(4)); grad = 0; break;
            default: coeficient = 0; grad = 0; break;
        }
        return new Monom(coeficient, grad);
    }
}
