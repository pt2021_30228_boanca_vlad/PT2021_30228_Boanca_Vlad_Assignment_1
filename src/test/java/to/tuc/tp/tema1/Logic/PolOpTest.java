package to.tuc.tp.tema1.Logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import to.tuc.tp.tema1.DataModels.Monom;
import to.tuc.tp.tema1.DataModels.Polinom;

import java.util.ArrayList;
import java.util.List;

public class PolOpTest {
    static Polinom p1 = new Polinom();
    static Polinom p2 = new Polinom();
    static Polinom p3 = new Polinom();
    static Polinom p4 = new Polinom();
    static Polinom p5 = new Polinom();
    static Polinom p6 = new Polinom();
    private static void initP() {

        Monom m1 = new Monom(1, 5);
        Monom m2 = new Monom(2, 3);
        Monom m3 = new Monom(2, 1);

        Monom m4 = new Monom(2, 4);
        Monom m5 = new Monom(1, 1);
        Monom m6 = new Monom(13, 0);

        Monom m7 = new Monom(4, 2);
        Monom m8 = new Monom(1, 1);
        Monom m9 = new Monom(20, 0);

        Monom m10 = new Monom(7, 3);
        Monom m11 = new Monom(1, 2);
        Monom m12 = new Monom(3, 1);

        Monom m13 = new Monom(1, 4);
        Monom m14 = new Monom(6, 3);
        Monom m15 = new Monom(7, 2);

        Monom m16 = new Monom(1, 6);
        Monom m17 = new Monom(1, 2);
        Monom m18 = new Monom(1, 1);
        p1.adaugaMonom(m1);
        p1.adaugaMonom(m2);
        p1.adaugaMonom(m3);
        p2.adaugaMonom(m4);
        p2.adaugaMonom(m5);
        p2.adaugaMonom(m6);
        p3.adaugaMonom(m7);
        p3.adaugaMonom(m8);
        p3.adaugaMonom(m9);
        p4.adaugaMonom(m10);
        p4.adaugaMonom(m11);
        p4.adaugaMonom(m12);
        p5.adaugaMonom(m13);
        p5.adaugaMonom(m14);
        p5.adaugaMonom(m15);
        p6.adaugaMonom(m16);
        p6.adaugaMonom(m17);
        p6.adaugaMonom(m18);
    }

    private static List<Arguments> adunaInput() {
        List<Arguments> arg = new ArrayList<>();
        arg.add(Arguments.of(p1, p2, "x^5 + 2x^4 + 2x^3 + 3x^1 + 13"));
        arg.add(Arguments.of(p2, p5, "3x^4 + 6x^3 + 7x^2 + x^1 + 13"));
        arg.add(Arguments.of(p4, p6, "x^6 + 7x^3 + 2x^2 + 4x^1"));

        return arg;
    }

    private static List<Arguments> scadeInput() {
        List<Arguments> arg = new ArrayList<>();
        arg.add(Arguments.of(p1, p2, "x^5 - 2x^4 + 2x^3 + x^1 - 13"));
        arg.add(Arguments.of(p2, p5, "x^4 - 6x^3 - 7x^2 + x^1 + 13"));
        arg.add(Arguments.of(p4, p6, " - x^6 + 7x^3 + 2x^1"));

        return arg;
    }

    private static List<Arguments> inmultesteInput() {
        List<Arguments> arg = new ArrayList<>();
        arg.add(Arguments.of(p1, p2, "2x^9 + 4x^7 + x^6 + 17x^5 + 2x^4 + 26x^3 + 2x^2 + 26x^1"));
        arg.add(Arguments.of(p2, p5, "2x^8 + 12x^7 + 14x^6 + x^5 + 19x^4 + 85x^3 + 91x^2"));
        arg.add(Arguments.of(p4, p6, "7x^9 + x^8 + 3x^7 + 7x^5 + 8x^4 + 4x^3 + 3x^2"));

        return arg;
    }

    private static List<Arguments> imparteInput() {
        List<Arguments> arg = new ArrayList<>();
        arg.add(Arguments.of(p2, p5, "2", " - 12x^3 - 14x^2 + x^1 + 13"));
        return arg;
    }

    private static List<Arguments> deriveazaInput() {
        List<Arguments> arg = new ArrayList<>();
        arg.add(Arguments.of(p1, "5x^4 + 6x^2 + 2"));
        arg.add(Arguments.of(p2, "8x^3 + 1"));
        arg.add(Arguments.of(p4, "21x^2 + 2x^1 + 3"));
        arg.add(Arguments.of(p5, "4x^3 + 18x^2 + 14x^1"));
        arg.add(Arguments.of(p6, "6x^5 + 2x^1 + 1"));
        return arg;
    }

    private static List<Arguments> integreazaInput() {
        List<Arguments> arg = new ArrayList<>();
        arg.add(Arguments.of(p1, "0.17x^6 + 0.50x^4 + x^2"));
        arg.add(Arguments.of(p2, "0.40x^5 + 0.50x^2 + 13x^1"));
        arg.add(Arguments.of(p4, "1.75x^4 + 0.33x^3 + 1.50x^2"));
        arg.add(Arguments.of(p5, "0.20x^5 + 1.50x^4 + 2.33x^3"));
        arg.add(Arguments.of(p6, "0.14x^7 + 0.33x^3 + 0.50x^2"));
        return arg;
    }

    private static List<Arguments> in() {
        List<Arguments> arg = new ArrayList<>();
        arg.add(Arguments.of());
        return arg;
    }

    @ParameterizedTest
    @MethodSource("in")
    public void init() {
        initP();
        Assertions.assertTrue(true);
    }

    @ParameterizedTest
    @MethodSource("adunaInput")
    public void aduna(Polinom p1, Polinom p2, String expectedResult) {
        Assertions.assertEquals(expectedResult, PolOp.aduna(p1, p2).toString());
    }

    @ParameterizedTest
    @MethodSource("scadeInput")
    public void scade(Polinom p1, Polinom p2, String expectedResult) {
        Assertions.assertEquals(expectedResult, PolOp.scade(p1, p2).toString());
    }

    @ParameterizedTest
    @MethodSource("inmultesteInput")
    public void inmulteste(Polinom p1, Polinom p2, String expectedResult) {
        Assertions.assertEquals(expectedResult, PolOp.inmulteste(p1, p2).toString());
    }

    @ParameterizedTest
    @MethodSource("imparteInput")
    public void imparte(Polinom p1, Polinom p2, String expectedCat, String expectedRest) {
        try {
            Polinom[] expected = PolOp.imparte(p1, p2);
            Assertions.assertEquals(expectedCat, expected[0].toString());
            Assertions.assertEquals(expectedRest, expected[1].toString());
        }catch (Exception e) {
            Assertions.fail();
        }
    }

    @ParameterizedTest
    @MethodSource("deriveazaInput")
//    @MethodSource("integreazaInput")
    public void deriveaza(Polinom p1, String expectedResult) {
        Assertions.assertEquals(expectedResult, PolOp.deriveaza(p1).toString());
//        Assertions.assertEquals(expectedResult, PolOp.integreaza(p1).toString());
    }

    @ParameterizedTest
    @MethodSource("integreazaInput")
    public void integreaza(Polinom p1, String expectedResult) {
        // aceasta metoda de test nu functioneaza, cel putin pe calculatorul meu. Am testa operatia de integrare
        // neparametrizat si a functionat, dar la aceasta metoda nu trece niciunul dintre teste, desi datele introduse in integreazaInput sunt corecte.
        // am incercat chiar sa inlocuiesc tot continutul metodei astfel incat sa testeze de fapt operatia de derivare
        // (metoda de mai sus, care functioneaza) si am primit tot o eroare.
        // se poate testa folosind metoda "deriveaza" de mai sus, comentand liniile pentru derivare si decomentandu-le pe cele pentru integrare.
        Assertions.assertEquals(expectedResult, PolOp.integreaza(p1).toString());
    }
}