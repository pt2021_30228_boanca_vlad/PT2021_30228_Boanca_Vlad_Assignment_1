package to.tuc.tp.tema1.Logic;

import org.junit.Test;
import to.tuc.tp.tema1.DataModels.Monom;

import static org.junit.Assert.*;

public class MonOpTest {

    Monom m1 = new Monom(-2, 1);
    Monom m2 = new Monom(-1, 1);

    @Test
    public void aduna() {
        String expectedResult = "3x^1"; // am omis "-" deoarece am implementat afisarea monoamelor in modul, dar verific semnul in assertTrue.
        Monom result = MonOp.aduna(m1, m2);
        String actualResult = result.toString();
        assertTrue(expectedResult.equals(actualResult) && result.getCoeficient().intValue() < 0);
    }

    @Test
    public void scade() {
        String expectedResult = "x^1"; // am omis "-" deoarece am implementat afisarea monoamelor in modul, dar verific semnul in assertTrue.
        Monom result = MonOp.scade(m1, m2);
        String actualResult = result.toString();
        assertTrue(expectedResult.equals(actualResult) && result.getCoeficient().intValue() < 0);
    }

    @Test
    public void inmulteste() {
        String expectedResult = "2x^2"; // am omis "-" deoarece am implementat afisarea monoamelor in modul, dar verific semnul in assertTrue.
        Monom result = MonOp.inmulteste(m1, m2);
        String actualResult = result.toString();
        assertTrue(expectedResult.equals(actualResult) && result.getCoeficient().intValue() >= 0);
    }

    @Test
    public void imparte() {
        String expectedResult = "2"; // am omis "-" deoarece am implementat afisarea monoamelor in modul, dar verific semnul in assertTrue.
        Monom result = MonOp.imparte(m1, m2);
        String actualResult = result.toString();
        assertTrue(expectedResult.equals(actualResult) && result.getCoeficient().intValue() >= 0);
    }
}